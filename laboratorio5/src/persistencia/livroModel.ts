import {Livro} from '../entidades/livro.js';
import {model, Schema, SchemaTypes} from 'mongoose';

const LivroSchema = new Schema<Livro>({
    titulo: { type: String, required: true },
    autores: [{ type: SchemaTypes.ObjectId, ref: 'Autor' }]
});

export const LivroModel = model<Livro>('Livro', LivroSchema, 'livros');
