import {Autor} from '../entidades/autor.js';
import {AutorModel} from './autorModel.js';

export async function criar(autor: Autor): Promise<Autor> {
    return AutorModel.create(autor); //retorna uma Promise
}

export async function buscar(): Promise<Autor[]> {
    let consulta = AutorModel.find();
    return consulta.exec(); //retorna uma Promise
}


export async function buscarArrayUltimo(autor:Autor): Promise<Autor[]>{
    let consulta = AutorModel.where('ultimo_nome', autor);
    return consulta.exec();
}

export async function buscarArrayPrimeiro(autor:Autor): Promise<Autor[]>{
    let consulta = AutorModel.where('primeiro_nome', autor);
    return consulta.exec();
}