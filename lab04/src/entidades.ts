class Moeda {
    constructor (
        private _valor: number,
        private _nome: string
    ){}

    get Valor(): number {
        return this._valor;
    }

    get Nome(): String {
        return this._nome;
    }
    
}

class Cofrinho {
    
    constructor(
        private _moedas: Moeda[] = []
    ){}

   adicionar(m:Moeda){
    this._moedas.push(m);
   }

   /* Versão utilizando forEach
   calcularTotal(){
    let total = 0;
    this._moedas.forEach((m:Moeda)=> total = total + m.Valor)
    return total;
   }
    */

    // Versão utilizando reduce

    calcularTotal(){
        return this._moedas.reduce(
            (total,moeda) => total + moeda.Valor, 0
        );
    }

}



export {Cofrinho, Moeda};