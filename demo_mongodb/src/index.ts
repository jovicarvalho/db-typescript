import { ClientRequest } from "http";
import { MongoClient } from "mongodb";
import { Pessoa } from "./entidades.js";    

const url = 'mongodb+srv://jovicarvalho:vrWkyg7iBktqXn7K@cluster0.wqch1k0.mongodb.net/?retryWrites=true&w=majority'
const cliente = new MongoClient(url)


try {
    console.log("Conectando com o banco de dados");
    await cliente.connect();
    console.log('Conectado com sucesso ao MongoDB');

    const db = cliente.db('demo_db');
    const colecao = db.collection('pessoas');

    console.log("Consultando documentos")
    const documentos =  await colecao.find<Pessoa >({}).toArray();
    console.log("resultado consulta");
    console.log(documentos);



} catch (error) { 
    console.log('Falha de acesso ao MongoDB');
    console.log(error);
} finally {
    await cliente.close();

}