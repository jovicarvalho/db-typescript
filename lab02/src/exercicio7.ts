// utilizando maps  e forEach


// primeiramente vamoos aprender a utilizar o MAP

/*const primeiroMapa = new Map();

primeiroMapa.set('jvcarvalho','João Vitor Nunes Carvalho');
primeiroMapa.set('duducadu','João Eduardo Nunes Carvalho');
primeiroMapa.set('Ade','Ademilde Castro Nunes');
// primeiroMapa.clear(); // Limpa o Mapa inteiro
// primeiroMapa.delete("duducadu"); Deleta um elemento do array;
console.log(primeiroMapa);
*/

//UTILIZANDO  O  forEach();

//const nomes = ['Whinds', 'Freeway', 'Teste', 'Maria'];

//nomes.forEach((nome, i) => {
//    console.log(nome + "Esse é um teste", i+1);
//})



/*
const mapeiaQuantidade = (array: number[]):Map<number,number> =>{
    const mapa = new Map<number,number>();
    array.forEach((valor)=>{
        if(!mapa.has(valor)){
            mapa.set(valor,1);
        }else{
            const contagem = mapa.get(valor);
            mapa.set(valor,contagem!+1)
        }
    })
    
    return mapa;

}


const arrayTeste = [1,2,3,4,5,55,3,2,2,2,1,4,6,7,];
console.log(mapeiaQuantidade(arrayTeste));

// Podemos perceber que no ForEach a a função que é passada na vdd funciona como  oarray e os parametros são seu indice e seu 

/*
function encontraQuantidade(array:number[]): Map<number,number{
    const tabela = new Map = <number, number> ();
    return;

}
*/




const vendoSeAprendiMesmo = (array:string[]): Map<string,number> =>{
    const tabela = new Map<string,number>();
    array.forEach((nome)=>{
        if(!tabela.has(nome)){
            tabela.set(nome,1);
        }else{
            let contagem = tabela.get(nome);
            tabela.set(nome,contagem!+1);
        }
    })

    return tabela;

}

const array =  ['João','João','João','João', 'Junior', 'Messias', 'Junior'];

console.log(vendoSeAprendiMesmo(array));