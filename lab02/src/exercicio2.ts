//* Qual o resultado do seguinte bloco de comandos?
let i: number = 0;
while (i != 10) {
console.log(i);
i += 0.2;
}
//RESPOSTA: o while gera um loop infinito
//* Qual a explicação para o resultado?
/*O resultado se da porque o number na verdade é um ponto flutuante
e enao deve ser usado quando quisermos trabalhar com exatidão.
Logo, o i nunca vai ser exatamente 10, gerando assim um loop infinito em JS"*/

