//* versão utilizando FOR 

let imprimirPares = (inicio:number, fim:number) => {
    if(inicio <0 || fim < 0){
        console.log("Digite valores positivos");
    }
    for(let contador = inicio; contador <=fim; contador++){
        if(contador % 2 == 0){
            console.log(contador);
        }
    }
}

imprimirPares(20,50);
imprimirPares(0,-2);
imprimirPares(0,100);

//* Versão Utilizando while


function paresWhile(){
    let inicio: number = 0;
    let fim : number = 50;
    while(inicio<=fim){
        if (inicio % 2 == 0){
        console.log(inicio);
        }
    inicio++;
    }

}
paresWhile();