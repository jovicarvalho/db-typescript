abstract class Cliente {
    constructor(
        private _nome: string
    ){}

    getNome(): string{
        return this._nome;
    }

    abstract getMensalidade():number;


} 

class ClienteFisico extends Cliente {
    
    constructor(_nome:string, private _idade: number, private _salario:number
    ){super(_nome)}

    getMensalidade():number {
        let mensalidade = this._salario * 0.1 ;
        if(this._idade >= 60 ){
            mensalidade = this._salario * 0.15
        };
        return mensalidade;
    }


    getIdade() {
        return this._idade
    }

    setIdade(idade:number) {
        idade = this._idade;
    }

    getSalario(){
        return this._salario;
    }

    setSalario(salario:number){
        this._salario = salario;
    }

}


class ClienteJuridico extends Cliente {
    constructor(getNome:string, private _mensalidade:number){
        super(getNome)
    }

    getMensalidade(): number {
       return this._mensalidade;
    }
    setMensalidade(men:number) {
        men = this._mensalidade;
    }
}


class CadastroClientes{
    constructor(
        private _cadastro: Cliente [] = []
    ){}

    setCliente(cliente:Cliente){
        this._cadastro.push(cliente);
    }

    getNomeMensalidade(){
        return this._cadastro.forEach((valor) =>{
             + console.log("Nome: " + valor.getNome()+ ", mensalidade: " + valor.getMensalidade())}
               ) 
        
    }

}


let clienteUm = new ClienteFisico("Fabricio Carvalho", 40, 1000);
let clienteDois = new ClienteFisico("Lilo",20,2000);
let clienteVelho = new ClienteFisico("Apeminondas",75,1000);
console.log(clienteUm.getMensalidade());

let bancoDeCadastro = new CadastroClientes;
bancoDeCadastro.setCliente(clienteUm);
bancoDeCadastro.setCliente(clienteDois);
bancoDeCadastro.setCliente(clienteVelho);

console.log(bancoDeCadastro.getNomeMensalidade());