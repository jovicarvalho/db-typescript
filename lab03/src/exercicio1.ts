class Circle {
    constructor(
        private _raio:number = 8,
        private _pontoX:number= 0,
        private _pontoY:number= 0,
    ){};
    
    get Raio() {
        return this._raio;
    }

    get Area(){
        return Math.PI * this._raio * this._raio;
    }

    get Comprimento(){
        return Math.PI * 2 * this._raio;
    }

}


const circulo = new Circle;
console.log(circulo.Area);
console.log(circulo.Comprimento);
