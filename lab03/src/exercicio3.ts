class Moeda {
    constructor (
        private _valor: number,
        private _nome: string
    ){}

    get Valor(): number {
        return this._valor;
    }

    get Nome(): String {
        return this._nome;
    }
    
}

class Cofrinho {

    constructor(
        private _moedas: Moeda[] = []
    ){}

   adicionar(m:Moeda){
    this._moedas.push(m);
   }

   /* Versão utilizando forEach
   calcularTotal(){
    let total = 0;
    this._moedas.forEach((m:Moeda)=> total = total + m.Valor)
    return total;
   }
    */

    // Versão utilizando reduce

    calcularTotal(){
        return this._moedas.reduce(
            (total,moeda) => total + moeda.Valor, 0
        );
    }

    acharMenor(){
        let menorMoeda:number = Infinity; 
        this._moedas.forEach((menor) => {
            if(menorMoeda > menor.Valor){
                menorMoeda = menor.Valor;
            }
        })
        return menorMoeda;
    }

}




const m1 = new Moeda(1, "um real");
const m2 = new Moeda(2,"dois reais");


const cofre = new Cofrinho;

cofre.adicionar(m1);
cofre.adicionar(m2);
console.log(cofre.acharMenor());